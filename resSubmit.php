<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$colleges = array('TCT' => 'Toán Cơ Tin Học', 'DL' => 'Địa lý', 'S' => 'Sinh', 'MT' => 'Môi trường');
$uploadDirectory = "./uploads/";
session_start();
$username = $_SESSION["username"];
$gender = $genders[$_SESSION["gender"]];
$college = $_SESSION["college"];
$dob = $_SESSION["dob"];
$address = $_SESSION["address"];
$img =  $uploadDirectory . $_SESSION["file"];

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="index.css">
    <title>Day 05: Xác nhận thông tin sinh viên</title>
</head>
<body>
<body>
    <form method="POST" action="">
        <div class="m-row">
            <div class="label">
                <label for="name">Họ và tên</label>
            </div>
            <div class=text>
                <?php echo "<p>$username</p>"; ?>
            </div>
        </div>
        <div class="m-row">
            <div class="label">
                <label for="gender">Giới tính</label>
            </div>
            <div class=text>
			    <?php echo "<p>$gender</p>"; ?>
            </div>
        </div>
		<div class="m-row">
            <div class="label">
                <label for="college">Phân khoa</label>
            </div>
            <div class=text>
                <?php echo "<p>$college</p>"; ?>
            </div>
        </div>
		<div class="m-row">
			<div class="label">
				<label for="dob">Ngày sinh</label>
			</div>
            <div class=text>
                <?php echo "<p>$dob</p>"; ?>
            </div>
		</div>
		<div class="m-row">
			<div class="label">
				<label for="address">Địa chỉ</label>
			</div>
            <div class=text>
                <?php echo "<p>$address</p>"; ?>
            </div>
		</div>
		<div class="box-img-show">
			<div class="label">
				<label for="image">Hình ảnh</label>
			</div>
            <div class="box-img">
            <?php 
				if (!empty($img)) {
					echo "<img src=\"$img\" width=\"100\" height=\"100\">";
				}
			?>
            </div>
		</div>
        <div class="submit-div">
            <input type="submit" value="Xác nhận"></input>
        </div>
    </form>
</body>
</html>
