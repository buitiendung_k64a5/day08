<?php
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
    <title>Day 08: Đăng ký tân sinh viên</title>
</head>
<body>
<?php
    session_start();
    if (isset($_POST['search']) && ($_POST['search'])) 
    {  
        if($_SESSION != $_POST){
            $_SESSION['colleges'] = $_POST['colleges'];
            $_SESSION['searchtext'] = $_POST['searchtext'];
        }   
    }
    if (isset($_POST['clearsearch']) && ($_POST['clearsearch']))
    {
        $_SESSION['colleges'] = '';
        $_SESSION['searchtext'] = '';
    }
	if (isset($_POST["moveToForm"])) {
        header('location: form.php');
    }
?>
<form method="POST" action="main.php" enctype="multipart/form-data" class="container">
    <div class="m-row between">
            <div class="label-main">
                <label for="college">Khoa</label>
            </div>
			<select name="colleges" id="colleges">
				<?php
                    $colleges = array('' => '', 'TCT' => 'Toán Cơ Tin Học', 'DL' => 'Địa lý', 'S' => 'Sinh', 'MT' => 'Môi trường');
					foreach ($colleges as $key => $value) {
						echo " <option ";
                            echo isset($_SESSION['colleges']) && $_SESSION['colleges'] == $key ? "selected " : "";
                        echo"                           
                        value =".$key.">".$value."</option> "; 
					}
				?>
			</select>
    </div>
    <div class="m-row between">
            <div class="label-main">
                <label for="name">Từ khóa</label>
            </div>
            <input class="text-input" type="text" id="searchtext" name="searchtext" value=<?php echo isset($_SESSION['searchtext']) ? $_SESSION['searchtext'] : ""; ?>>
    </div>
    <div class="submit-div" style="margin: 35px 0; text-align: center">
        <input type="submit" id='clearsearch' name='clearsearch' class="btn" onclick='clearSearchField()' value="Xóa"></button>
        <input type="submit" id='search' name='search' class="btn" value="Tìm kiếm"></button>
    </div>
    <div class="m-row" style="margin: 15px 0;">
        <p>Số sinh viên tìm thấy: XXX</p>
    </div>
    <div class="submit-div" style="text-align: right; padding-right: 148px">
        <button class="btn" name="moveToForm">Thêm</button>
    </div>
    <div>
    <table>
        <tr>
            <th>No</th>
            <th>Tên sinh viên</th>
            <th>Khoa</th>
            <th>Action</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Nguyễn Văn A</td>
            <td>Khoa học máy tính</td>
            <td class="m-row-feat">
            <a href="#" class="btn">Xóa</a>
            <a href="#" class="btn">Sửa</a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Trần Thị B</td>
            <td>Khoa học máy tính</td>
            <td class="m-row-feat">
            <a href="#" class="btn">Xóa</a>
            <a href="#" class="btn">Sửa</a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Nguyễn Hoàng C</td>
            <td>Khoa học vật liệu</td>
            <td class="m-row-feat">
            <a href="#" class="btn">Xóa</a>
            <a href="#" class="btn">Sửa</a>
            </td>
        </tr>
        <tr>
            <td>4</td>
            <td>Đinh Quang D</td>
            <td>Khoa học vật liệu</td>
            <td class="m-row-feat">
            <a href="#" class="btn">Xóa</a>
            <a href="#" class="btn">Sửa</a>
            </td>
        </tr>
    </table>
    </div>
</form>
<script>
        function clearSearchField() {
            document.getElementById("colleges").value = "";
            document.getElementById("searchtext").value = "";
        }
    </script>
</body>
<style>
    form.container{
        width: 775px;  
        margin: 0 auto;
		padding: 20px 10px;
    }
    form table{
        width: 850px;
        text-align: left;
        margin:15px 0 0 35px;
    }
    .m-row {
		margin: 10px auto;
		width: 410px;
		display: flex;
		min-height: 30px;
	}
    .m-row .between{
        justify-content: space-between;
    }
    .label-main{
		min-width: 100px;
		padding: 5px;
		margin-right: 20px;
		height: 18px;
	}
    .btn{
        color: white;
		padding: 10px 25px;
		border: 1px solid blue;
		border-radius: 5px;
        background-color: rgb(68, 68, 204);
		text-decoration: none;
    }
    select {
		border: 1px solid blue;
		width: 176px;
	}
    .m-row-feat{
        display: flex;
        justify-content: start;
    }
    .m-row-feat >.btn{
        margin: 5px;
        background-color: rgb(120, 120, 196);
    }
</style>

</html>